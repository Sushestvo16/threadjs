import { createReducer } from '@reduxjs/toolkit';
import { setPosts, addMorePosts, addPost, setExpandedPost, deletePosts } from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true,
  success: false
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(deletePosts, (state, action) => {
    const { post } = action.payload;

    state.posts = state.posts.filter(postA => postA.id !== post.id);
    state.success = true;
  });
});

export { reducer };
