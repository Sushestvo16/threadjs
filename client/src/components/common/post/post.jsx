import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label } from 'src/components/common/common';
import { useState } from 'react';
import styles from './styles.module.scss';

const Post = ({ post, onPostLike, onPostDislike, onPostDelete, onExpandedPostToggle, sharePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const [liked, setLiked] = useState(false);
  const [likes, setLikes] = useState(Number(likeCount));
  const [disliked, setDisliked] = useState(false);
  const [dislikes, setDislikes] = useState(Number(dislikeCount));

  const handlePostLike = () => {
    setLiked(!liked);
    if (disliked) {
      setDisliked(!disliked);
      setDislikes(dislikes - 1);
      onPostDislike(id);
    }
    if (liked === true) {
      setLikes(likes - 1);
      onPostLike(id);
      return;
    }
    setLikes(likes + 1);
    onPostLike(id);
  };
  const handlePostDelete = () => {
    onPostDelete(id);
  };
  const handlePostDislike = () => {
    setDisliked(!disliked);
    if (liked) {
      setLiked(!liked);
      setLikes(likes - 1);
      onPostLike(id);
    }
    if (disliked === true) {
      setDislikes(dislikes - 1);
      onPostDislike(id);
      return;
    }
    setDislikes(dislikes + 1);
    onPostDislike(id);
  };

  const handleExpandedPostToggle = () => onExpandedPostToggle(id);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {Number(likes)}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={handlePostDislike}>
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikes}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => handlePostDelete(id)}
        >
          <Icon name={IconName.DELETE} />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired
};

export default Post;
