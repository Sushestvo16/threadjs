import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import { useSelector } from 'react-redux';
import { useState } from 'react';
import styles from './styles.module.scss';
import { Card, Label } from '../../../common/common';
import { IconName } from '../../../../common/enums/components/icon-name.enum';
import { dislikeComment, likeComment } from '../../../../store/thread/actions';

const Comment = ({ comment: { body, createdAt, user } }) => {
  const { posts } = useSelector(state => ({
    posts: state.posts.posts
  }));
  console.log(posts);
  console.log(body);

  const [liked, setLiked] = useState(false);
  const [likes, setLikes] = useState(Number(1));
  const [disliked, setDisliked] = useState(false);
  const [dislikes, setDislikes] = useState(Number(2));

  const handleCommentLike = id => {
    setLiked(!liked);
    if (disliked) {
      setDisliked(!disliked);
      setDislikes(dislikes - 1);
      dislikeComment(id);
    }
    if (liked === true) {
      setLikes(likes - 1);
      likeComment(id);
      return;
    }
    setLikes(likes + 1);
    likeComment(id);
  };
  const handleCommentDislike = id => {
    setDisliked(!disliked);
    if (liked) {
      setLiked(!liked);
      setLikes(likes - 1);
      likeComment(id);
    }
    if (disliked === true) {
      setDislikes(dislikes - 1);
      dislikeComment(id);
      return;
    }
    setDislikes(dislikes + 1);
    dislikeComment(id);
  };
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <CommentUI.Text>{body}</CommentUI.Text>
      </CommentUI.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleCommentLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {Number(likes)}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={handleCommentDislike}>
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikes}
        </Label>
      </Card.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired
};

export default Comment;
