import { ApiPath } from '../common/enums/enums.js';
import { auth, user, comment, post, image } from '../services/services.js';
import { initAuth } from './auth/auth.api.js';
import { initPost } from './post/post.api.js';
import { initComment } from './comment/comment.api.js';
import { initImage } from './image/image.api.js';

// register all routes
const initApi = Router => {
  const apiRouter = Router();

  apiRouter.use(
    ApiPath.AUTH,
    initAuth(Router, {
      auth,
      user
    })
  );
  apiRouter.use(
    ApiPath.POSTS,
    initPost(Router, {
      post
    })
  );
  apiRouter.use(
    ApiPath.COMMENTS,
    initComment(Router, {
      comment
    })
  );
  apiRouter.use(
    ApiPath.IMAGES,
    initImage(Router, {
      image
    })
  );

  return apiRouter;
};

export { initApi };
