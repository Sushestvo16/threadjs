import pkg from 'sequelize';
const { DataTypes } = pkg;

const init = orm => {
  const PostReaction = orm.define(
    'postReaction',
    {
      isLike: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return PostReaction;
};

export { init };
