import pkg from 'sequelize';
const { DataTypes } = pkg;

const init = orm => {
  const Post = orm.define(
    'post',
    {
      body: {
        allowNull: false,
        type: DataTypes.TEXT
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return Post;
};

export { init };
