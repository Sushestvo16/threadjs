import pkg from 'sequelize';
const { DataTypes } = pkg;

const init = orm => {
  const Image = orm.define(
    'image',
    {
      link: {
        allowNull: false,
        type: DataTypes.STRING
      },
      deleteHash: {
        allowNull: false,
        type: DataTypes.STRING
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return Image;
};

export { init };
