import { sequelize as orm } from '../db/connection.js';
import associate from '../db/associations.js';
import { init as initUserModel } from './user/user.model.js';
import { init as initPostModel } from './post/post.model.js';
import { init as initPostReactionModel } from './post-reaction/post-reaction.model.js';
import { init as initCommentModel } from './comment/comment.model.js';
import { init as initImageModel } from './image/image.model.js';

const User = initUserModel(orm);
const Post = initPostModel(orm);
const PostReaction = initPostReactionModel(orm);
const Comment = initCommentModel(orm);
const Image = initImageModel(orm);

associate({
  User,
  Post,
  PostReaction,
  Comment,
  Image
});

export {
  User as UserModel,
  Post as PostModel,
  PostReaction as PostReactionModel,
  Comment as CommentModel,
  Image as ImageModel
};
