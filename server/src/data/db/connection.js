import Sequelize from 'sequelize';
import * as dbConfig from '../../config/config.js';

const sequelize = new Sequelize(dbConfig);

export { sequelize };
