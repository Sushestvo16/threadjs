import {
  CommentModel,
  UserModel,
  ImageModel,
  PostModel,
  PostReactionModel
} from '../models/index.js';
import { Comment } from './comment/comment.repository.js';
import { Image } from './image/image.repository.js';
import { PostReaction } from './post-reaction/post-reaction.repository.js';
import { Post } from './post/post.repository.js';
import { User } from './user/user.repository.js';

const comment = new Comment({
  commentModel: CommentModel,
  userModel: UserModel,
  imageModel: ImageModel
});

const image = new Image({
  imageModel: ImageModel
});

const postReaction = new PostReaction({
  postReactionModel: PostReactionModel,
  postModel: PostModel
});

const post = new Post({
  postModel: PostModel,
  commentModel: CommentModel,
  userModel: UserModel,
  imageModel: ImageModel,
  postReactionModel: PostReactionModel
});

const user = new User({
  userModel: UserModel,
  imageModel: ImageModel
});

export { comment, image, postReaction, post, user };
