const socketInjector = io => (req, res, next) => {
  req.io = io;
  next();
};

export { socketInjector };
