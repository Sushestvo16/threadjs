"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _fs = _interopRequireDefault(require("fs"));

var _express = _interopRequireWildcard(require("express"));

var _path = _interopRequireDefault(require("path"));

var _passport = _interopRequireDefault(require("passport"));

var _http = _interopRequireDefault(require("http"));

var _socket = _interopRequireDefault(require("socket.io"));

var _cors = _interopRequireDefault(require("cors"));

var _constants = require("./common/constants/constants.js");

var _enums = require("./common/enums/enums.js");

var _connection = require("./data/db/connection.js");

var _api = require("./api/api.js");

var _middlewares = require("./middlewares/middlewares.js");

var _handlers = require("./socket/handlers.js");

require("./config/passport.js");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])();

var socketServer = _http["default"].Server(app);

var io = (0, _socket["default"])(socketServer);

_connection.sequelize.authenticate().then(function () {
  console.info('Connection has been established successfully.');
})["catch"](function (err) {
  console.error('Unable to connect to the database:', err);
});

io.on('connection', _handlers.handlers);
app.use((0, _cors["default"])());
app.use(_express["default"].json());
app.use(_express["default"].urlencoded({
  extended: true
}));
app.use(_passport["default"].initialize());
app.use((0, _middlewares.socketInjector)(io));
app.use(_enums.ENV.APP.API_PATH, (0, _middlewares.authorization)(_constants.WHITE_ROUTES));
app.use(_enums.ENV.APP.API_PATH, (0, _api.initApi)(_express.Router));

var _dirname = _path["default"].resolve();

var staticPath = _path["default"].resolve("".concat(_dirname, "/../client/build"));

app.use(_express["default"]["static"](staticPath));
app.get('*', function (req, res) {
  res.write(_fs["default"].readFileSync("".concat(_dirname, "/../client/build/index.html")));
  res.end();
});
app.use(_middlewares.errorHandler);
app.listen(_enums.ENV.APP.PORT, function () {
  console.info("Server listening on port ".concat(_enums.ENV.APP.PORT, "!"));
});
socketServer.listen(_enums.ENV.APP.SOCKET_PORT);
